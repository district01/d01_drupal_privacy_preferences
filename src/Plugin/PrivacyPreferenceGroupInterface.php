<?php

namespace Drupal\d01_drupal_privacy_preferences\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Privacy preference group plugins.
 */
interface PrivacyPreferenceGroupInterface extends PluginInspectionInterface {

  /**
   * Get the cookie listing.
   *
   * @return array
   *   An array of cookie names.
   */
  public function getCookieList();

  /**
   * Get the cookie listing.
   *
   * @return bool
   *   A boolean to indicate whether the cookie group is required.
   */
  public function isRequired();

}
