<?php

namespace Drupal\d01_drupal_privacy_preferences\Plugin\PrivacyPreferenceGroup;

use Drupal\d01_drupal_privacy_preferences\PrivacyPreferenceGroupBase;

/**
 * Defines a default plugin for essential privacy preference.
 *
 * @PrivacyPreferenceGroup(
 *   id = "essential",
 *   label = @Translation("Essential"),
 * )
 */
class Essential extends PrivacyPreferenceGroupBase {

  /**
   * {@inheritdoc}
   */
  public function getCookieList() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isRequired() {
    return FALSE;
  }

}
