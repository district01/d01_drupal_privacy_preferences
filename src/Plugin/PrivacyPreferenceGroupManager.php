<?php

namespace Drupal\d01_drupal_privacy_preferences\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Privacy preference group plugin manager.
 */
class PrivacyPreferenceGroupManager extends DefaultPluginManager {


  /**
   * Constructs a new PrivacyPreferenceGroupManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/PrivacyPreferenceGroup', $namespaces, $module_handler, 'Drupal\d01_drupal_privacy_preferences\Plugin\PrivacyPreferenceGroupInterface', 'Drupal\d01_drupal_privacy_preferences\Annotation\PrivacyPreferenceGroup');

    $this->alterInfo('d01_drupal_privacy_preferences_privacy_preference_group_info');
    $this->setCacheBackend($cache_backend, 'd01_drupal_privacy_preferences_privacy_preference_group_plugins');
  }

}
