<?php

namespace Drupal\d01_drupal_privacy_preferences\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Privacy preference group plugins.
 */
abstract class PrivacyPreferenceGroupBase extends PluginBase implements PrivacyPreferenceGroupInterface {

  /**
   * {@inheritdoc}
   */
  public function getCookieList() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isRequired() {
    return FALSE;
  }

}
