<?php

namespace Drupal\d01_drupal_privacy_preferences\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Privacy preference group entities.
 */
interface PrivacyPreferenceGroupInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
