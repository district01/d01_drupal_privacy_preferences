<?php

namespace Drupal\d01_drupal_privacy_preferences\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Privacy preference group entity.
 *
 * @ConfigEntityType(
 *   id = "privacy_preference_group",
 *   label = @Translation("Privacy preference group"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\d01_drupal_privacy_preferences\PrivacyPreferenceGroupListBuilder",
 *     "form" = {
 *       "add" = "Drupal\d01_drupal_privacy_preferences\Form\PrivacyPreferenceGroupForm",
 *       "edit" = "Drupal\d01_drupal_privacy_preferences\Form\PrivacyPreferenceGroupForm",
 *       "delete" = "Drupal\d01_drupal_privacy_preferences\Form\PrivacyPreferenceGroupDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\d01_drupal_privacy_preferences\PrivacyPreferenceGroupHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "privacy_preference_group",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/privacy_preference_group/{privacy_preference_group}",
 *     "add-form" = "/admin/structure/privacy_preference_group/add",
 *     "edit-form" = "/admin/structure/privacy_preference_group/{privacy_preference_group}/edit",
 *     "delete-form" = "/admin/structure/privacy_preference_group/{privacy_preference_group}/delete",
 *     "collection" = "/admin/structure/privacy_preference_group"
 *   }
 * )
 */
class PrivacyPreferenceGroup extends ConfigEntityBase implements PrivacyPreferenceGroupInterface {

  /**
   * The Privacy preference group ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Privacy preference group label.
   *
   * @var string
   */
  protected $label;

}
