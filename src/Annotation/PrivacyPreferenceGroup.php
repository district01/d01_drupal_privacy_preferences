<?php

namespace Drupal\d01_drupal_privacy_preferences\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Privacy preference group item annotation object.
 *
 * @see \Drupal\d01_drupal_privacy_preferences\Plugin\PrivacyPreferenceGroupManager
 * @see plugin_api
 *
 * @Annotation
 */
class PrivacyPreferenceGroup extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
