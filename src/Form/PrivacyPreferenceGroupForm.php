<?php

namespace Drupal\d01_drupal_privacy_preferences\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PrivacyPreferenceGroupForm.
 */
class PrivacyPreferenceGroupForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;
    kint($entity);

    $privacy_preference_group = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $privacy_preference_group->label(),
      '#description' => $this->t("Label for the Privacy preference group."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $privacy_preference_group->id(),
      '#machine_name' => [
        'exists' => '\Drupal\d01_drupal_privacy_preferences\Entity\PrivacyPreferenceGroup::load',
      ],
      '#disabled' => !$privacy_preference_group->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => '',
      '#description' => $this->t('Additional explanation about the privacy preference group.'),
    ];

    $options = [];
    $type = \Drupal::service('plugin.manager.privacy_preference_group');
    $plugin_definitions = $type->getDefinitions();
    foreach ($plugin_definitions as $plugin_definition) {
      kint($plugin_definition);
      $options[$plugin_definition['id']] = $plugin_definition['label'];
    }

    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Privacy preference group'),
      '#options' => $options,
      '#default_value' => '',
      '#description' => $this->t('Additional explanation about the privacy preference group.'),
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $privacy_preference_group = $this->entity;
    $status = $privacy_preference_group->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Privacy preference group.', [
          '%label' => $privacy_preference_group->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Privacy preference group.', [
          '%label' => $privacy_preference_group->label(),
        ]));
    }
    $form_state->setRedirectUrl($privacy_preference_group->toUrl('collection'));
  }

}
